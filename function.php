<?php
// fungsi untuk meng-ekstrak konten di antara 2 teks.
function extract_content($text, $start_text, $end_text) {
  if (empty($text)) return false;

  $start = strpos($text, $start_text);
  if ($start === false) return false;
  $start += strlen($start_text);

  $end = strpos($text, $end_text, $start);
  if ($end === false) return false;

  $length = $end - $start;
  return substr($text, $start, $length);
}
?>