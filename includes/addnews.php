<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Tambah Berita</title>
 </head>

 <body>
  <form method="post" action=".?page=addnews_process">
   <fieldset class="fields">
    <legend>Data Berita Baru</legend>
    <ul>
     <li>
      <label for="newstitle">Judul</label>
      <input type="text" name="newstitle" id="newstitle">
     </li>
     <li>
      <label for="newstags">Tag</label>
      <input type="text" name="newstags" id="newstags">
     </li>
     <li>
      <label for="newscontent">Konten</label>
      <textarea name="newscontent" id="newscontent" cols=50 rows=8></textarea>
     </li>
    </ul>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="simpan" value="Simpan">
    <input type="submit" name="batal" value="Batal">
   </fieldset>
  </form>
 </body>
</html>