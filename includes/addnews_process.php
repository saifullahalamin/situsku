<?php
// cek apakah yang di-klik tombol simpan.
if (isset($_POST["simpan"])) {
  // ambil data dari form.
  $userid = mysql_real_escape_string($_SESSION["userid"]);
  $newstitle = mysql_real_escape_string($_POST["newstitle"]);
  $newstags = mysql_real_escape_string($_POST["newstags"]);
  $newscontent = mysql_real_escape_string(nl2br($_POST["newscontent"]));

  // cek validasi data.
  if (empty($userid)) die("user-ID tidak boleh kosong!");
  if (empty($newstitle)) die("Judul berita tidak boleh kosong!");
  if (empty($newstags)) die("Tag berita tidak boleh kosong!");
  if (empty($newscontent)) die("harus ada berita yang disampaikan!");

  // insert data ke database (tabel news).
  $sql = "insert into news (news_title, news_tags, news_content, user_id) 
     values (\"$newstitle\", \"$newstags\",\"$newscontent\", \"$userid\")";
  $result = mysql_query($sql, $dbh);
  if (!$result) die("Gagal menyimpan data!");

   // jika sudah selesai, masuk ke halaman newslist.
  if ($usergroup != "administrator") {
	   header("Location: .?page=newslist");
   }
  if ($usergroup = "administrator") {
	   header("Location: .?page=newsadmin");
   }
}
// jika klik batal, balik ke Beranda.
else header("Location: .");
?>