<?php
// cegah user selain administrator utk mengakses halaman ini.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Tambah User</title>
 </head>

 <body>
  <form method="post" action=".?page=adduser_process">
   <fieldset class="fields">
    <legend>Data User Baru</legend>
    <ul>
     <li>
      <label for="userid">User ID</label>
      <input type="text" name="userid" id="userid">
     </li>
     <li>
      <label for="passwd">Password</label>
      <input type="password" name="passwd" id="passwd">
     </li>
     <li>
      <label for="username">Nama User</label>
      <input type="text" name="username" id="username">
     </li>
     <li>
      <label for="usergroup">Grup</label>
      <select name="usergroup" id="usergroup">
        <option value="anggota">Anggota</option>
        <option value="kontributor">Kontributor</option>
        <option value="administrator">Administrator</option>
      </select>
     </li>
    </ul>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="simpan" value="Simpan">
    <input type="submit" name="batal" value="Batal">
   </fieldset>
  </form>
 </body>
</html>