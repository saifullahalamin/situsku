<?php
// cegah user selain administrator utk mengakses halaman ini.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");

// cek apakah yang di-klik tombol simpan.
if (isset($_POST["simpan"])) {
  // ambil data dari form.
  $userid = mysql_real_escape_string($_POST["userid"]);
  $passwd = mysql_real_escape_string($_POST["passwd"]);
  $username = mysql_real_escape_string($_POST["username"]);
  $usergroup = mysql_real_escape_string($_POST["usergroup"]);

  // cek validasi data.
  if (empty($userid)) die("User-ID tidak boleh kosong!");
  if (strlen($userid) > 20) die("User-ID tidak boleh lebih dari 20 huruf!");
  if (empty($passwd)) die("Password tidak boleh kosong!");
  if (empty($username)) die("Nama tidak boleh kosong!");

  // insert data ke database (tabel user).
  $sql = "insert into user values (\"$userid\", md5(\"$passwd\"),
    \"$username\", now(), \"$usergroup\")";
  $result = mysql_query($sql, $dbh);
  if (!$result) die("Gagal menyimpan data!");

  // jika bukan administrator, simpan ke tabel member.
  if ($usergroup != "administrator") {
    $sql = "insert into member values (\"$userid\", \"$username\", null, null)";
    $result = mysql_query($sql, $dbh);
  }

  // selesai menyimpan, masuk ke halaman Admin User.
  header("Location: .?page=useradmin");
}
// jika klik batal, balik ke Beranda.
else header("Location: .");
?>