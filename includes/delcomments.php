<?php
// cegah user selain administrator utk mengakses halaman ini.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");

// error jika ID user tidak ada.
if (empty($_GET["id"])) die("Parameter kurang!");

// ambil data komentar dari database.
$comid = mysql_real_escape_string($_GET["id"]);
$sql = "select * from comments where com_id = \"$comid\"";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$data = mysql_fetch_assoc($result);
if (!$data) die("Komentar dengan ID tersebut tidak ditemukan!");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Hapus Komentar</title>
 </head>

 <body>
  <form method="post" action=".?page=delcomments_process">
   <input type="hidden" name="comid" value="<?php echo($comid) ?>">
   <fieldset class="fields">
     <legend>Detail Komentar</legend>
    <ul>
     <li>
      <label>ID Komentar</label>
      <?php echo($comid) ?>
     </li>
     <li>
      <label>Tgl Komentar</label>
      <?php echo($data["com_time"]) ?>
     </li>
     <li>
      <label>Nama Kmenttor</label>
      <?php echo($data["com_name"]) ?>
     </li>
     <li>
      <label>E-mail</label>
      <?php echo($data["com_email"]) ?>
     </li>
     <li>
      <label>Website</label>
      <?php echo($data["com_website"]) ?>
     </li>
     <li>
      <label>Isi Komentar</label>
      <?php echo($data["com_content"]) ?>
     </li>
     <li>
      <label>ID-Berita</label>
      <?php echo($data["news_id"]) ?>
     </li>
    </ul>
    <p>Anda yakin akan menghapus komentar di atas?</p>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="hapus" value="Hapus Komentar">
    <input type="submit" name="batal" value="Batal">
   </fieldset>
  </form>
 </body>
</html>