<?php
// cegah user selain administrator utk mengakses halaman ini.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");

// error jika ID user tidak ada.
if (empty($_GET["id"])) die("Parameter kurang!");

// ambil data member dari database.
$userid = mysql_real_escape_string($_GET["id"]);
$sql = "select * from member where user_id = \"$userid\"";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$data = mysql_fetch_assoc($result);
if (!$data) die("User dengan ID tersebut tidak ditemukan!");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Hapus Member</title>
 </head>

 <body>
  <form method="post" action=".?page=delmember_process">
   <input type="hidden" name="userid" value="<?php echo($userid) ?>">
   <fieldset class="fields">
    <legend>Data Member</legend>
    <ul>
     <li>
      <label>User ID</label>
      <?php echo($userid) ?>
     </li>
	 <li>
      <label>Nama Member</label>
      <?php echo($data["member_name"]) ?>
     </li>
     <li>
      <label>Tgl Lahir</label>
      <?php echo($data["member_birthdate"]) ?>
     </li>
     <li>
      <label>Alamat</label>
      <?php echo($data["member_address"]) ?>
     </li>
    </ul>
    <p>Anda yakin akan menghapus data member di atas?</p>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="hapus" value="Hapus">
    <input type="submit" name="batal" value="Batal">
   </fieldset>
  </form>
 </body>
</html>