<?php
// cegah user selain administrator utk mengakses halaman ini.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");

// error jika ID user tidak ada.
if (empty($_GET["id"])) die("Parameter kurang!");

// ambil data berita dari database.
$newsid = mysql_real_escape_string($_GET["id"]);
$sql = "select * from news where news_id = \"$newsid\"";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$data = mysql_fetch_assoc($result);
if (!$data) die("berita dengan ID tersebut tidak ditemukan!");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Hapus Berita</title>
 </head>

 <body>
  <form method="post" action=".?page=delnews_process">
   <input type="hidden" name="newsid" value="<?php echo($newsid) ?>">
   <fieldset class="fields">
    <legend>Data Berita</legend>
    <ul>
     <li>
      <h1> <?php echo($data["news_title"]) ?> </h1>
     </li>
     <li>
      <?php echo($data["news_content"]) ?>
     </li>
    </ul>
    <p>Anda yakin akan menghapus berita di atas?</p>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="hapus" value="Hapus">
    <input type="submit" name="batal" value="Batal">
   </fieldset>
  </form>
 </body>
</html>