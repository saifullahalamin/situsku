<?php
// cegah user selain administrator utk mengakses halaman ini.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");

// error jika ID user tidak ada.
if (empty($_GET["id"])) die("Parameter kurang!");

// ambil data user dari database.
$userid = mysql_real_escape_string($_GET["id"]);
$sql = "select * from user where user_id = \"$userid\"";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$data = mysql_fetch_assoc($result);
if (!$data) die("User dengan ID tersebut tidak ditemukan!");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Hapus User</title>
 </head>

 <body>
  <form method="post" action=".?page=deluser_process">
   <input type="hidden" name="userid" value="<?php echo($userid) ?>">
   <fieldset class="fields">
    <legend>Data User</legend>
    <ul>
     <li>
      <label>User ID</label>
      <?php echo($userid) ?>
     </li>
     <li>
      <label>Nama User</label>
      <?php echo($data["user_name"]) ?>
     </li>
     <li>
      <label>Grup</label>
      <?php echo(ucwords($data["user_group"])) ?>
     </li>
    </ul>
    <p>Anda yakin akan menghapus data user di atas?</p>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="hapus" value="Hapus">
    <input type="submit" name="batal" value="Batal">
   </fieldset>
  </form>
 </body>
</html>