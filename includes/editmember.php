<?php
// cegah user selain administrator utk mengakses halaman ini.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");

// error jika ID user tidak ada.
if (empty($_GET["id"])) die("Parameter kurang!");

// ambil data member dari database.
$userid = mysql_real_escape_string($_GET["id"]);
$sql = "select * from member where user_id = \"$userid\"";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$data = mysql_fetch_assoc($result);
if (!$data) die("User dengan ID tersebut tidak ditemukan!");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Edit Member</title>
 </head>

 <body>
  <form method="post" action=".?page=editmember_process">
   <fieldset class="fields">
   <input type="hidden" name="userid" id="userid" value="<?php echo($data["user_id"]) ?>">
    <legend>Data Member</legend>
    <ul>
     <li>
      <label>User ID</label>
      <?php echo($userid) ?>
     </li>
     <li>
      <label for="membername">Nama Member</label>
      <input type="text" name="membername" id="membername" value="<?php echo($data["member_name"]) ?>">
     </li>
     <li>
      <label for="memberbirthdate">Tgl Lahir</label>
      <input type="text" name="memberbirthdate" id="memberbirthdate" value="<?php echo($data["member_birthdate"]) ?>">
     </li>
     <li>
      <label for="memberaddress">Alamat</label>
      <input type="text" name="memberaddress" id="memberaddress" value="<?php echo($data["member_address"]) ?>">
     </li>
    </ul>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="simpan" value="Simpan">
    <input type="submit" name="batal" value="Batal">
   </fieldset>
  </form>
 </body>
</html>