<?php
// cegah user selain administrator utk mengakses halaman ini.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");

// error jika ID user tidak ada.
if (empty($_GET["id"])) die("Parameter kurang!");

// ambil data berita dari database.
$newsid = mysql_real_escape_string($_GET["id"]);
$sql = "select * from news where news_id = \"$newsid\"";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$data = mysql_fetch_assoc($result);
if (!$data) die("User dengan ID tersebut tidak ditemukan!");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Edit Berita</title>
 </head>

 <body>
  <form method="post" action=".?page=editnews_process">
   <input type="hidden" name="newsid" value="<?php echo($newsid) ?>">
   <fieldset class="fields">
    <legend>Data Berita</legend>
    <ul>
     <li>
      <label for="newstitle">Judul</label>
      <input type="text" name="newstitle" id="newstitle" value='<?php echo($data["news_title"]) ?>'>
     </li>
     <li>
      <label for="newstags">Tags</label>
      <input type="text" name="newstags" id="newstags" value='<?php echo($data["news_tags"]) ?>'>
     </li>
     <li>
      <label for="userid">Penulis berita</label>
      <input type="text" name="userid" id="userid" value='<?php echo($data["user_id"]) ?>'>
     </li>
     <li>
	  <label for="newscontent">Konten</label>
      <textarea name="newscontent" id="newscontent" cols=45 rows=8><?php echo($data["news_content"]) ?> </textarea>
      </select>
     </li>
    </ul>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="simpan" value="Simpan">
    <input type="submit" name="batal" value="Batal">
   </fieldset>
  </form>
 </body>
</html>