<?php
// cegah user selain administrator utk mengakses halaman ini.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");

// error jika ID user tidak ada.
if (empty($_GET["id"])) die("Parameter kurang!");

// ambil data user dari database.
$userid = mysql_real_escape_string($_GET["id"]);
$sql = "select * from user where user_id = \"$userid\"";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$data = mysql_fetch_assoc($result);
if (!$data) die("User dengan ID tersebut tidak ditemukan!");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Edit User</title>
 </head>

 <body>
  <form method="post" action=".?page=edituser_process">
   <input type="hidden" name="userid" value="<?php echo($userid) ?>">
   <fieldset class="fields">
    <legend>Data User</legend>
    <ul>
     <li>
      <label>User ID</label>
      <?php echo($userid) ?>
     </li>
     <li>
      <label for="username">Nama User</label>
      <input type="text" name="username" id="username" value='<?php echo($data["user_name"]) ?>'>
     </li>
     <li>
      <label for="usergroup">Grup</label>
      <select name="usergroup" id="usergroup">
        <option value="anggota"<?php if ($data["user_group"] == "anggota") echo(" selected") ?>>Anggota</option>
        <option value="kontributor"<?php if ($data["user_group"] == "kontributor") echo(" selected") ?>>Kontributor</option>
        <option value="administrator"<?php if ($data["user_group"] == "administrator") echo(" selected") ?>>Administrator</option>
      </select>
     </li>
    </ul>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="simpan" value="Simpan">
    <input type="submit" name="batal" value="Batal">
   </fieldset>
  </form>
 </body>
</html>