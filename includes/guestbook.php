<?php
// ambil jumlah data yang ada.
$sql = "select count(*) from guestbook";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$row = mysql_fetch_row($result);
$total_data = $row[0];

// hitung jumlah halaman.
$item_per_hal = 5;
$jml_hal = ceil($total_data / $item_per_hal);

// cek apakah nomor halaman ada, jika tdk anggap hal 1.
if (isset($_GET["no"]))
  $halaman = $_GET["no"];
else $halaman = 0;

// jika no hal melebihi jml hal, anggap hal trakhir.
if ($halaman >= $jml_hal) $halaman = $jml_hal - 1;

// ambil indeks pertama yg muncul.
$awal = $item_per_hal * $halaman;

// ambil data sebenarnya hanya utk halaman yg tampil.
$sql = "select * from guestbook order by guest_time desc
          limit $awal, $item_per_hal";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");

// masukkan ke dalam array.
$daftar = array();
while ($row = mysql_fetch_assoc($result)) {
  $daftar[] = $row;
}
mysql_free_result($result);

// jika belum ada user, tampilkan error.
if (count($daftar) == 0) die("Belum ada tamu!");

// buat navigasi halaman.
$navigasi = array();
for ($i = 0; $i < $jml_hal; $i++) {
  $hal = $i + 1;
  if ($halaman == $i) $navigasi[] = $hal;
  else $navigasi[] = "<a href=\".?page=guestbook&no=$i\">$hal</a>";
}
$navigasi = implode(", ", $navigasi);
?>
<html>
<head>
  <title>Buku Tamu</title>
</head>

<body>
  <table class="daftar">
    <caption>Buku Tamu</caption>
    <thead>
      <tr>
        <th>No</th>
        <th>Waktu</th>
        <th>Nama</th>
        <th>Komentar</th>
      </tr>
    </thead>
    <tbody>
<?php
// tampilkan data per baris.
foreach ($daftar as $no => $data) {
  $no += $awal + 1;
  echo "<tr>
          <td>$no</td>
          <td>$data[guest_time]</td>
          <td>$data[guest_name]</td>
          <td>$data[guest_comment]</td>  
        </tr>";
}
?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan=7>
          Halaman ke-<?php echo($halaman + 1) ?> dari <?php echo($jml_hal) ?> halaman.
          Lihat halaman: <?php echo($navigasi) ?>
        </td>
      </tr>
    </tfoot>
  </table>
  <form method="POST" action=".?page=writeguestbook">
    <fieldset class="buttons">
      <input type="submit" name="isi" value="Isi Buku Tamu">
    </fieldset>
  </form>
</body>
</html>