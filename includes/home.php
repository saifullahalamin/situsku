<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Strict//EN">
<html>
 <head>
  <title>Beranda</title>
 </head>

 <body>

<?php
// ambil jumlah data yang ada.
$sql = "select count(*) from news";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$row = mysql_fetch_row($result);
$total_data = $row[0];

// hitung jumlah halaman.
$item_per_hal = 10;
$jml_hal = ceil($total_data / $item_per_hal);

// cek apakah nomor halaman ada, jika tdk anggap hal 1.
if (isset($_GET["no"]))
  $halaman = $_GET["no"];
else $halaman = 0;

// jika no hal melebihi jml hal, anggap hal trakhir.
if ($halaman >= $jml_hal) $halaman = $jml_hal - 1;

// ambil indeks pertama yg muncul.
$awal = $item_per_hal * $halaman;

// ambil data sebenarnya hanya utk halaman yg tampil.
$sql = "SELECT news.news_id, news.news_time, news.news_title, news.news_tags, news.news_content,
            news.user_id, user.user_name
		FROM news, user
		WHERE news.user_id = user.user_id
		ORDER BY news_time DESC
        limit $awal, $item_per_hal";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");

// masukkan ke dalam array.
$daftar = array();
while ($row = mysql_fetch_assoc($result)) {
  $daftar[] = $row;
}
mysql_free_result($result);

// jika belum ada user, tampilkan error.
if (count($daftar) == 0) die("Belum ada user!");

// buat navigasi halaman.
$navigasi = array();
for ($i = 0; $i < $jml_hal; $i++) {
  $hal = $i + 1;
  if ($halaman == $i) $navigasi[] = $hal;
  else $navigasi[] = "<a href=\".?page=useradmin&no=$i\">$hal</a>";
}

$navigasi = implode(", ", $navigasi);

// tampilkan data per baris.
foreach ($daftar as $no => $data) {
  $no += $awal + 1;
  $first = ucfirst(substr("$data[news_content]",0,1));
  $berita = substr("$data[news_content]",1,90);
  echo "
		  <div class=\"galeriberita\">
		  <ul>
		  <li> <a href=\".?page=viewnews&id=$data[news_id]\">
		  <h1 class=\"news\">$data[news_title]</h1>
		  <p class=\"t\">Ditulis Oleh : $data[user_name] 
		  <br> Tags : $data[news_tags]
		  <br> Pada : $data[news_time]</p>
          <p class=\"newscontent\"> $first$berita ...</p></a>
		  </li>
		  </ul>
		  </div>
		  ";
		  
}
?>

<div class="berita"> * berita yang ditampilkan adalah sepuluh berita terbaru, 
 klik <a href=?page=newslist&no=1>disini</a> untuk melihat berita yang lain. </div>
 </body>
</html>