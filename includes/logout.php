<?php
// kosongkan session.
$_SESSION["userid"] = "";
$_SESSION["username"] = "";
$_SESSION["usergroup"] = "";

// hapus cookie.
setcookie("userid", "", time() - 60);
setcookie("username", "", time() - 60);
setcookie("usergroup", "", time() - 60);

// keluar ke halaman login.
header("Location: .?page=login");
?>