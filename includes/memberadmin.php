<?php
// cek apakah yg login seorang administrator.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");

// ambil jumlah data yang ada.
$sql = "select count(*) from member";
$result = mysql_query($sql, $dbh);
$row = mysql_fetch_row($result);
$total_data = $row[0];

// hitung jumlah halaman.
$item_per_hal = 3;
$jml_hal = ceil($total_data / $item_per_hal);

// cek apakah nomor halaman ada, jika tdk anggap hal 1.
if (isset($_GET["no"]))
  $halaman = $_GET["no"];
else $halaman = 0;

// jika no hal melebihi jml hal, anggap hal trakhir.
if ($halaman >= $jml_hal) $halaman = $jml_hal - 1;

// ambil indeks pertama yg muncul.
$awal = $item_per_hal * $halaman;

// ambil data sebenarnya hanya utk halaman yg tampil.
$sql = "select * from member order by member_name
          limit $awal, $item_per_hal";
$result = mysql_query($sql, $dbh);

// masukkan ke dalam array.
$daftar = array();
while ($row = mysql_fetch_assoc($result)) {
  $daftar[] = $row;
}
mysql_free_result($result);

// jika belum ada member, tampilkan error.
if (count($daftar) == 0) echo("Belum ada memeber!");

// buat navigasi halaman.
$navigasi = array();
for ($i = 0; $i < $jml_hal; $i++) {
  $hal = $i + 1;
  if ($halaman == $i) $navigasi[] = $hal;
  else $navigasi[] = "<a href=\".?page=memberadmin&no=$i\">$hal</a>";
}
$navigasi = implode(", ", $navigasi);
?>
<html>
<head>
  <title>Kelola User</title>
</head>

<body>
  <table class="daftar">
    <caption>Daftar Member</caption>
    <thead>
      <tr>
        <th>No</th>
        <th>User ID</th>
        <th>Nama</th>
		<th>Alamat</th>
        <th colspan=3>Aksi</th>
      </tr>
    </thead>
    <tbody>
<?php
// tampilkan data per baris.
foreach ($daftar as $no => $data) {
  $no += $awal + 1;
  echo "<tr>
          <td>$no</td>
          <td>$data[user_id]</td>
          <td>$data[member_name]</td>
		  <td>$data[member_address]</td>
          <td><a href=\".?page=viewmember&id=$data[user_id]\">Lihat</a></td>
          <td><a href=\".?page=editmember&id=$data[user_id]\">Edit</a></td>
          <td><a href=\".?page=delmember&id=$data[user_id]\">Hapus</a></td>
        </tr>";
}
?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan=7>
          Halaman ke-<?php echo($halaman + 1) ?> dari <?php echo($jml_hal) ?> halaman.
          Lihat halaman: <?php echo($navigasi) ?>
        </td>
      </tr>
    </tfoot>
  </table>
</body>
</html>