<?php
// cek apakah sudah login.
if (empty($_SESSION["userid"]))
  die("Anda harus login dulu!");

// error jika ID user tidak ada.
if (empty($_GET["id"])) die("Parameter kurang!");

// ambil data user dari database.
$comid = mysql_real_escape_string($_GET["id"]);
$sql = "select * from comments where com_id = \"$comid\"";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$data = mysql_fetch_assoc($result);
if (!$data) die("komentar dengan ID tersebut tidak ditemukan!");

if ($_SESSION["usergroup"] == "administrator")
  $action = "commentsadmin";
else $action = "newslist";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Detail Komentar (<?php echo $data["com_name"] ?>)</title>
 </head>

 <body>
  <form method="post" action=".?page=<?php echo $action ?>">
   <fieldset class="fields">
    <legend>Detail Komentar</legend>
    <ul>
     <li>
      <label>ID Komentar</label>
      <?php echo($comid) ?>
     </li>
     <li>
      <label>Tgl Komentar</label>
      <?php echo($data["com_time"]) ?>
     </li>
     <li>
      <label>Nama Kmenttor</label>
      <?php echo($data["com_name"]) ?>
     </li>
     <li>
      <label>E-mail</label>
      <?php echo($data["com_email"]) ?>
     </li>
     <li>
      <label>Website</label>
      <?php echo($data["com_website"]) ?>
     </li>
     <li>
      <label>Isi Komentar</label>
      <?php echo($data["com_content"]) ?>
     </li>
     <li>
      <label>ID-Berita</label>
      <?php echo($data["news_id"]) ?>
     </li>
    </ul>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="ok" value="OK">
   </fieldset>
  </form>
 </body>
</html>