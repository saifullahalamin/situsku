<?php
// cegah user selain administrator utk mengakses halaman ini.
if ($_SESSION["usergroup"] != "administrator")
  die("Halaman ini hanya boleh diakses oleh seorang administrator!");

// cek apakah sudah login.
if (empty($_SESSION["userid"]))
  die("Anda harus login dulu!");

// error jika ID user tidak ada.
if (empty($_GET["id"])) die("Parameter kurang!");

// ambil data user dari database.
$userid = mysql_real_escape_string($_GET["id"]);
$sql = "select * from member where user_id = \"$userid\"";
$result = mysql_query($sql, $dbh);
if (!$result) die("Belum ada member!");
$data = mysql_fetch_assoc($result);
if (!$data) die("User dengan ID tersebut tidak ditemukan!");

if ($_SESSION["usergroup"] == "administrator")
  $action = "memberadmin";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Detail Member (<?php echo $data["member_name"] ?>)</title>
 </head>

 <body>
  <form method="post" action=".?page=<?php echo $action ?>">
   <fieldset class="fields">
    <legend>Data Member</legend>
    <ul>
     <li>
      <label>User ID</label>
      <?php echo($userid) ?>
     </li>
     <li>
      <label>Nama Member</label>
      <?php echo($data["member_name"]) ?>
     </li>
     <li>
      <label>Tgl Lahir</label>
      <?php echo($data["member_birthdate"]) ?>
     </li>
     <li>
      <label>Alamat</label>
      <?php echo($data["member_address"]) ?>
     </li>
    </ul>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="ok" value="OK">
   </fieldset>
  </form>
 </body>
</html>