<?php
// cek apakah sudah login.
if (empty($_SESSION["userid"]))
  die("Anda harus login dulu!");

// error jika ID user tidak ada.
if (empty($_GET["id"])) die("Parameter kurang!");

// ambil data user dari database.
$userid = mysql_real_escape_string($_GET["id"]);
$sql = "select * from user where user_id = \"$userid\"";
$result = mysql_query($sql, $dbh);
if (!$result) die("Gagal melakukan query");
$data = mysql_fetch_assoc($result);
if (!$data) die("User dengan ID tersebut tidak ditemukan!");

if ($_SESSION["usergroup"] == "administrator")
  $action = "useradmin";
else $action = "userlist";
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Detail User (<?php echo $data["user_name"] ?>)</title>
 </head>

 <body>
  <form method="post" action=".?page=<?php echo $action ?>">
   <fieldset class="fields">
    <legend>Data User</legend>
    <ul>
     <li>
      <label>User ID</label>
      <?php echo($userid) ?>
     </li>
     <li>
      <label>Nama User</label>
      <?php echo($data["user_name"]) ?>
     </li>
     <li>
      <label>Tanggal Terdaftar</label>
      <?php echo($data["user_regdate"]) ?>
     </li>
     <li>
      <label>Grup</label>
      <?php echo(ucwords($data["user_group"])) ?>
     </li>
    </ul>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="ok" value="OK">
   </fieldset>
  </form>
 </body>
</html>