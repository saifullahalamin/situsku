<?php
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
 <head>
  <title>Isi Buku Tamu</title>
 </head>

 <body>
  <form method="post" action=".?page=writeguestbook_process">
   <fieldset class="fields">
    <legend>Isi Buku Tamu</legend>
    <ul>
     <li>
      <label for="guestname">Nama</label>
      <input type="text" name="guestname" id="guestname">
     </li>
     <li>
      <label for="guestemail">E-mail</label>
      <input type="text" name="guestemail" id="guestemail">
     </li>
	 <li>
      <label for="guestwebsite">Website</label>
      <input type="text" name="guestwebsite" id="guestwebsite"> (optional)
     </li>
     <li>
      <label for="guestcomment">Komentar</label>
      <textarea name="guestcomment" id="guestcomment" cols=30 rows=4></textarea>
     </li>
    </ul>
   </fieldset>
   <fieldset class="buttons">
    <input type="submit" name="simpan" value="Simpan">
    <input type="submit" name="batal" value="Batal">
   </fieldset>
  </form>
 </body>
</html>