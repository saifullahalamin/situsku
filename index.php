<?php
// ambil konfigurasi aplikasi web.
require_once("config.php");
// inisialisasi session.
require_once("session.php");
// load fungsi-fungsi penting.
require_once("function.php");

// melakukan koneksi ke database server.
$dbh = mysql_connect($CFG["db_host"], $CFG["db_user"], $CFG["db_pass"]);
if (!$dbh) die("Gagal konek ke database!");
$sel = mysql_select_db($CFG["db_name"], $dbh);
if (!$sel) die("Database tidak ditemukan/belum dibuat!");

// aktifkan output buffering.
ob_start();

// ambil konten sesuai parameter di URL.
if (empty($_GET["page"])) $_GET["page"] = "home";
@include("includes/$_GET[page].html");
@include("includes/$_GET[page].php");
@include("lirycs/$_GET[page].html");
@include("lirycs/$_GET[page].php");
$content = ob_get_contents();
ob_clean();

if (!empty($content)) {
  $title = extract_content($content, "<title>", "</title>");
  $content = extract_content($content, "<body>", "</body>");
}

// ambil template theme utk membungkus konten.
include("theme.html");
$html = ob_get_contents();
ob_clean();

require_once("menu.php");
foreach ($MENU as $teks => $link)
  $menus .= "<li><a href=\"$link\">$teks</a></li>";
$html = str_replace("{MAIN-MENU}", $menus, $html);

//mengambil data inisial lirik lagu 
require_once("lirycs/lirycsinit.php");
$html = str_replace("{LIRYCS-INIT}", $array2div, $html);

//mengambil script share to facebook
require_once("lirycs/share2fb.php");
$html = str_replace("{SHARE2FB}", $share2fb, $html);

if (!empty($title))
  $html = str_replace("{PAGE-TITLE}", "$CFG[site_abbr] - $title", $html);
else $html = str_replace("{PAGE-TITLE}", $CFG["site_title"], $html);

$html = str_replace("{SITE-TITLE}", $CFG["site_title"], $html);
$html = str_replace("{PAGE-CONTENT}", $content, $html);

if (empty($_SESSION["userid"]))
  $visitor = "silahkan <a href=\".?page=login\">login</a>";
else $visitor = $_SESSION["username"];
$html = str_replace("{VISITOR}", $visitor, $html);


// akhiri output buffering.
ob_end_clean();

// tutup koneksi ke database.
mysql_close($dbh);

// tampilkan hasil akhir.
echo $html;
?>