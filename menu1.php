<?php
// menu default.
$MENU = array(
  'Beranda'			  => '.',
  'Login'			  => '.?page=login',
  'Mendaftar'		  => '.?page=signup',
  'Daftar Berita'	  => '.?page=newslist',
  'Buku Tamu'		  => '.?page=guestbook',
  'Perihal'			  => '.?page=about',
);

// jika login sebagai admin.
if ($_SESSION['usergroup'] == 'administrator') {
  $MENU['Berita Baru'] = '.?page=addnews';
  $MENU['User Baru'] = '.?page=adduser';
  $MENU['Kelola Berita'] = '.?page=newsadmin';
  $MENU['Kelola User'] = '.?page=useradmin';
  $MENU['Kll Member'] = '.?page=memberadmin';
  $MENU['Kll Komentar'] = '.?page=commentsadmin';
  unset($MENU['Daftar Berita']);
  unset($MENU['Perihal']);
}
// selain itu, asal sudah login.
else if (!empty($_SESSION['userid'])) {
  $MENU['Daftar User'] = '.?page=userlist';
  $MENU['Berita Baru'] = '.?page=addnews';
}

// menu kalo sudah login.
if (!empty($_SESSION['userid'])) {
  unset($MENU['Login']);
  unset($MENU['Mendaftar']);
  unset($MENU['Buku Tamu']);
  $MENU['Logout'] = '.?page=logout';
}
?>