-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 17, 2012 at 01:10 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `paw`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `com_id` int(11) NOT NULL AUTO_INCREMENT,
  `com_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `com_name` varchar(45) NOT NULL,
  `com_email` varchar(50) NOT NULL,
  `com_website` varchar(100) DEFAULT NULL,
  `com_content` text NOT NULL,
  `news_id` int(11) NOT NULL,
  PRIMARY KEY (`com_id`),
  KEY `news_id` (`news_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`com_id`, `com_time`, `com_name`, `com_email`, `com_website`, `com_content`, `news_id`) VALUES
(1, '2012-01-06 15:51:06', 'ipul', 'ipul@ymail.com', NULL, 'bagus', 14),
(3, '2012-01-07 06:38:57', 'ipul lagi', 'saifullah@ymail.com', '', 'alhamdulillah yaahhh.', 14),
(6, '2012-01-07 06:54:00', 'ipul lagi', 'saifullah@ymail.com', '', 'coba dikomentari', 13),
(7, '2012-01-07 06:56:41', 'ipul lagi', 'saifullah@ymail.com', '', 'ayo di komen lagi', 13),
(8, '2012-01-07 07:03:27', 'silvi', 'silvi@gmail.com', '', 'mw ikut nguli', 10),
(9, '2012-01-07 19:41:42', 'ipul', 'ipul@ymail.com', '', 'teruskan pah', 16),
(10, '2012-01-08 14:05:16', 'saifullah', 'saifullah@ymail.com', '', 'coba di komen', 12);

-- --------------------------------------------------------

--
-- Table structure for table `guestbook`
--

CREATE TABLE IF NOT EXISTS `guestbook` (
  `guest_id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `guest_name` varchar(45) NOT NULL,
  `guest_email` varchar(35) NOT NULL,
  `guest_website` varchar(100) DEFAULT NULL,
  `guest_comment` text,
  PRIMARY KEY (`guest_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `guestbook`
--

INSERT INTO `guestbook` (`guest_id`, `guest_time`, `guest_name`, `guest_email`, `guest_website`, `guest_comment`) VALUES
(1, '2012-01-05 12:50:18', 'yudha', 'yudha@ymail.com', NULL, 'horeee'),
(2, '2012-01-05 13:17:14', 'lufi', 'lufi@ymail.com', 'cs.co.cc', 'boleh lah'),
(3, '2012-01-05 13:18:27', 'lia', 'lia@gmail.com', '', 'salam kenal dari lia'),
(7, '2012-01-05 13:59:08', 'santhi', 'santhi@ymail.com', '', 'ihirrrrr'),
(8, '2012-01-06 09:16:21', 'ipul', 'ipul@ymail.com', '', 'alhamdulillah yaa hampir jadi.'),
(9, '2012-01-06 09:37:40', 'kopiko', 'kopiko@gmail.com', '', 'buat tombo ngantuk.'),
(10, '2012-01-07 08:44:23', 'cobi', 'cobi@ymail.com', '', 'kagem admin sugeng kartoraharjo');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `user_id` varchar(20) NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `member_birthdate` date NOT NULL,
  `member_address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `news_title` varchar(100) NOT NULL,
  `news_tags` varchar(100) NOT NULL,
  `news_content` text NOT NULL,
  `user_id` varchar(20) NOT NULL,
  PRIMARY KEY (`news_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_time`, `news_title`, `news_tags`, `news_content`, `user_id`) VALUES
(9, '2012-01-06 07:36:14', 'kodok', 'bangkak', 'bisa merayap di dinding walau gak pake alat pengaman.', 'admin'),
(10, '2012-01-06 07:35:22', 'Calon Gedung Tertinggi ke-5 Dunia Dibangun di SCBD Jakarta ', 'tertinggi', 'Indonesia diprediksi akan memiliki gedung tertinggi kelima dunia setinggi 638 meter sebelum 2020. Menara ini akan dibangun di SCDB, Jakarta. detiknews', 'admin'),
(11, '2012-01-06 07:38:45', '50 Best Chocolate Desserts – Your Ultimate Chocolate Dessert Guide …', 'coklat', 'Mmm. The Best Chocolate Desserts! I honestly believe that chocolate is the closest we can get to heaven as mortals. Seriously. Is there anything better than chocolate to make you feel all wonderful and gooey inside? Can anything cheer you up as instantly as chocolate desserts can? If you’re like me, you are going to love these Best Chocolate Desserts as well as the mouth-watering pictures of the amazing treats and the detailed cooking recipes provided for all 50 chocolate desserts. So, what are we waiting for? Let’s dig in! You can either pick the chocolate desserts you are interested in from the list provided below or simply go to next page and start viewing all these fantastic top chocolate desserts one by one! Whichever your chocolate-loving heart prefers! Enjoy!', 'admin'),
(12, '2012-01-06 12:11:55', 'Text Color', 'Text Color', 'The color property is used to set the color of the text.\r\n\r\nWith CSS, a color is most often specified by:\r\n\r\n    <br> - a HEX value - like "#ff0000"    \r\n    <br> - an RGB value - like "rgb(255,0,0)"   \r\n    <br> - a color name - like "red" \r\n   ', 'admin'),
(13, '2012-01-14 12:31:47', 'Styling Links bagian2', 'link', 'Links can be styled with any CSS property (e.g. color, font-family, background, etc.).\r\n<br>\r\n<br>Special for links are that they can be styled differently depending on what state they are in.\r\n<br>\r\n<br>The four links states are:\r\n<br>\r\n<br>    a:link - a normal, unvisited link\r\n<br>    a:visited - a link the user has visited\r\n<br>    a:hover - a link when the user mouses over it\r\n<br>    a:active - a link the moment it is clicked\r\n ', 'admin'),
(14, '2012-01-07 08:20:15', 'Populerkan Blog Kita Dengan Facebook', 'fb', 'NOTE: Cek disini untuk cara menambahkan Facebook Like dan Facebook Comment tanpa plugin.\r\n\r\nAku sebenarnya baru mencoba cara ini 2 hari yang lalu, tapi jumlah trafik dalam 2 hari ini ternyata meningkat cukup lumayan. Dari webstats terlihat kalau peningkatan trafik ini datangnya dari Facebook.\r\n\r\nYang ku lakukan sederhana aja, cuma menambahkan tombol Facebook Like, Share dan fitur Connect with Facebook di form komentar. Fitur ini memudahkan pengunjung untuk meninggalkan komentar tanpa perlu menuliskan nama dan email lagi, cukup mengklik satu tombol dan dia otomatis dapat berkomentar via akun Facebooknya. ', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` varchar(20) NOT NULL,
  `user_pass` char(32) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_regdate` datetime NOT NULL,
  `user_group` enum('administrator','kontributor','anggota') NOT NULL DEFAULT 'anggota',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_pass`, `user_name`, `user_regdate`, `user_group`) VALUES
('adkhan', 'e5ec6e1b0625b562034f976baaeb764c', 'Adkhan Sholeh', '2011-12-29 15:40:39', 'anggota'),
('admin', '9b8c99fb6e4dc9808702d92338d8c40f', 'Administrator', '2011-12-29 00:00:00', 'administrator'),
('anonymous', '1f2bf2e032a11fec0ea5d7f9d6a5aa1d', 'anonymous', '2012-01-08 00:33:20', 'anggota'),
('arca', 'a61523d971b70f20e620e4a781b4d117', 'Ari Cahyono', '2011-12-29 15:38:21', 'kontributor'),
('asnawi', '72a017834ee67dfe507d6d9edb74493a', 'Choerun Asnawi', '2011-12-29 15:37:17', 'kontributor'),
('bekti', '4414fc94b2a1a771b1cd4faa8ec12cfc', 'Dayat Subekti', '2011-12-29 15:38:07', 'anggota'),
('fifi', '2430242dc52b9fec75095457ac808899', 'fifah', '2012-01-07 23:39:38', 'anggota'),
('hehe', '529ca8050a00180790cf88b63468826a', 'herman', '2012-01-07 23:44:37', 'anggota'),
('hihi', 'e9f5713dec55d727bb35392cec6190ce', 'himawan', '2012-01-07 23:46:07', 'anggota'),
('ifah', 'af6dd2a6af0b4c2b4f588428e936efd3', 'ifah', '2012-01-08 02:17:36', 'anggota'),
('ipul', 'db3fd70d6207f588571a26e91d744baf', 'saifullah al amin', '2012-01-07 23:38:55', 'anggota');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
