<?php
session_start();

if (empty($_SESSION["userid"])) {
  if (isset($_COOKIE["userid"])) {
    $_SESSION["userid"] = $_COOKIE["userid"];
    $_SESSION["username"] = $_COOKIE["username"];
    $_SESSION["usergroup"] = $_COOKIE["usergroup"];
  }
}
?>